FROM ruby:alpine

MAINTAINER Jeremy VAILLANT <vaillant.jeremy@dev-crea.com>

ENV SERVICE_NAME "BDHerve"
ENV RAILS_ENV "production"
ENV SECRET_KEY_BASE rake secret
ENV RAILS_SERVER_STATIC_FILES true

RUN apk add --update --no-cache yarn libssl1.0 nodejs tzdata \
  && apk add --virtual build-dependencies make g++ gcc \
  && rm -rf /var/cache/apk/*

WORKDIR /$SERVICE_NAME

ADD . /$SERVICE_NAME/

RUN bundle install --without local \
  && yarn init -y \
  && apk del build-dependencies \
  && rake assets:precompile

EXPOSE 3000

CMD bundle exec puma
